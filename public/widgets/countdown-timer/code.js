if (window._staticWidgetCountdownTimerCount === undefined) {
	window._staticWidgetCountdownTimerCount = 1;
	Array.from(document.getElementsByClassName('static-widget-countdown-timer')).forEach(function (widget) {
		let end = new Date(widget.dataset.endtime);
		let _second = 1000;
		let _minute = _second * 60;
		let _hour = _minute * 60;
		let _day = _hour * 24;
		let timer;

		let timeLeft = {};
		function showRemaining() {
			const now = new Date();
			const distance = end - now;
			if (distance < 0) {
				clearInterval(timer);
			} else {
				timeLeft = {
					days: Math.floor(distance / _day),
					hours: Math.floor((distance % _day) / _hour),
					minutes: Math.floor((distance % _hour) / _minute),
					seconds: Math.floor((distance % _minute) / _second),
				};
			}
			for (const period in timeLeft) {
				widget.querySelector(`.static-widget-countdown-timer-${period}-counter`).innerText =
					timeLeft[period];
			}
		}
		timer = setInterval(showRemaining, 1000);
	}
)
} else {
	window._staticWidgetCountdownTimerCount += 1
}
