if (window._staticWidgetNumberCounterCount === undefined) {
	window._staticWidgetNumberCounterCount = 1;
	console.log('loaded')
	Array.from(document.getElementsByClassName('static-widget-percent-counter')).forEach(function (widget) {
		const el = widget.querySelector('.static-widget-percent-counter-completed');
		el.style.width = el.dataset.completed + '%';
	})
}else  {
	window._staticWidgetNumberCounterCount += 1
}
