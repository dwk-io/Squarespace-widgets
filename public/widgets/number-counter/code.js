if (window._staticWidgetNumberCounterCount === undefined) {
	window._staticWidgetNumberCounterCount = 1;
	Array.from(document.getElementsByClassName('static-widget-number-counter')).forEach(function (widget) {
		const animationDuration = 2000;
		const frameDuration = 1000 / 60;
		const totalFrames = Math.round( animationDuration / frameDuration );
		function easeOutQuad (t) { return t * (2 - t); };
		const el = widget.querySelector('.static-widget-number-counter-count')
		let frame = 0;
		const countTo = Number.parseInt(widget.dataset.count);
		const counter = setInterval( () => {
			frame++;
			const progress = easeOutQuad(frame / totalFrames);
			const currentCount = Math.round(countTo * progress);
			if (Number.parseInt( el.innerText, 10 ) !== currentCount) {
				el.innerText = currentCount;
			}
			if (frame >= totalFrames) {
				clearInterval(counter);
			}
		}, frameDuration );
	})
}else  {
	window._staticWidgetNumberCounterCount += 1
}
