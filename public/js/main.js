//TODO: dynamically build the list of widgets
const widgets = ["Countdown Timer", "Number Counter", "Percent Counter"];

// const supportedInputs = {
//   color: {
//     htmlInput: "color",
//   },
// };

let currentWidget;

function dasherize(word) {
  return word.toLowerCase().replaceAll(" ", "-");
}

async function fetchWidget(name) {
  const filename = dasherize(name);
  const config = await fetch(`widgets/${filename}/config.json`).then(
    (response) => response.json()
  );
  config.id = {
    value: (Math.random() + 1).toString(36).substring(7),
    type: "text",
  };

  const template = await fetch(`widgets/${filename}/html.handlebars`).then(
    (response) => response.text()
  );

  const javascript = {
    source: `widgets/${filename}/code.js`,
    code: await fetch(`widgets/${filename}/code.js`).then(
      (response) => response.text()
    )
  }

  return {
    config,
    template,
    javascript,
    name: name,
  };
}

function minify(code) {
  return code.replaceAll(
    /\s+/g,
    " "
  ).replaceAll(/;\n/g, ';')
}

function generateWidgetCode() {
  const html = Handlebars.compile(currentWidget.template)(currentWidget.config);
  const demoEl = document.getElementById("widget-demo");
  //TODO: code should be minified
  document.getElementById("widget-code").innerText = `<!-- BEGIN ${
    currentWidget.name
  } ${currentWidget.config.id.value} -->\n${minify(html)}<script>${minify(currentWidget.javascript.code)}</script>\n<!-- END ${currentWidget.name} ${currentWidget.config.id.value} -->`;
  while (demoEl.firstChild) {
    demoEl.removeChild(demoEl.firstChild);
  }
  const script = document.createElement('script');
  script.src =currentWidget.javascript.source;
  demoEl.insertAdjacentHTML('afterbegin', html);
  demoEl.appendChild(script);
}

function regenerateWidgetCode(event) {
  const changedEl = event.target;
  currentWidget.config[changedEl.name].value = changedEl.value;
  console.log(
    `Updated currentWidget.config.${changedEl.name}.value to ${changedEl.value}`
  );
  generateWidgetCode();
}

function generateWidgetEditor(currentWidget) {
  const widgetConfigEl = document.getElementById("widget-config");
  widgetConfigEl.innerHTML = '';
  for (const key in currentWidget.config) {
    const labelEl = document.createElement("label");
    const inputEl = document.createElement("input");
    inputEl.type = currentWidget.config[key].type;
    inputEl.name = key;
    inputEl.value = currentWidget.config[key].value;
    inputEl.addEventListener("change", regenerateWidgetCode);
    labelEl.htmlFor = key;
    labelEl.innerText = `${key}: `;
    widgetConfigEl.appendChild(labelEl);
    widgetConfigEl.appendChild(inputEl);
    widgetConfigEl.appendChild(document.createElement("br"));
  }
}

async function loadWidget(event) {
  currentWidget = await fetchWidget(event.target.value);
  generateWidgetEditor(currentWidget);
  generateWidgetCode();
}

function main() {
  const widgetSelectorEl = document.getElementById("widget-selector");
  widgets.forEach((widget) => {
    let optionEl = document.createElement("option");
    optionEl.value = widget;
    optionEl.innerHTML = widget;
    widgetSelectorEl.appendChild(optionEl);
  });
  widgetSelectorEl.addEventListener("change", loadWidget);
}

window.addEventListener("load", main);
